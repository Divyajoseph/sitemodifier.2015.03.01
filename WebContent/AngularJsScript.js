﻿//// declare a module
var myAppModule = angular.module('myApp', ['ngRoute']);

// in this example we will create a greeting filter
myAppModule.filter('greet', function () {
    return function (name) {
     
        return 'Hello, ' + name + '!';
    };
});



myAppModule.directive('ngInitial', function () {
    return {
        restrict: 'A',
        controller: [
          '$scope', '$element', '$attrs', '$parse', function ($scope, $element, $attrs, $parse) {
              var getter, setter, val;
              val = $attrs.ngInitial || $attrs.value;
              getter = $parse($attrs.ngModel);
              setter = getter.assign;
              setter($scope, val);
          }
        ]
    };
});


myAppModule.directive('showonhoverparent', function() {
    return {
           
           //link : function(scope, element, attrs) {
           //    element.parent().bind('mouseenter', function() {
           //        element.show();
           //    });
           //    element.parent().bind('mouseleave', function() {
           //        element.hide();
           //    });
           //}
       };
   });


myAppModule.controller('MainCtrl', function ( $scope,
          $rootScope,
          $http) {
    $scope.name = 'World';
    $scope.msg = 'initial msg';
    $scope.count = 0;
    $scope.affectedControlId = "";
    $scope.editorElementId = "";
    $scope.visible = true;
    $scope.NgShowEditorIdLogo = false;
    $scope.NgShowEditorIdHome = false;

    $scope.ModelHomeText = "someText";
    $scope.ModelHomeHref = "SomeHref";

    $scope.go = function (item) {
        alert("From GO");
    }

    $scope.getItem = function () {
        alert("From getItem");
        $scope.count = $scope.count + 1;
        $scope.msg = 'modified msg ' + $scope.count;         
    }
           

    $scope.toggle = function () {
          $scope.visible = !$scope.visible;
    }


    $scope.uploadFile2 = function(files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);
        var uploadUrl = "www.google.com";
        $http.post(uploadUrl, fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(alert(" ...all right!... ")).error(alert(" ..damn!... "));

    };

    $scope.uploadFile = function () {
        var f = document.getElementById('file').files[0],
            r = new FileReader();
        r.onloadend = function (e) {
            var data = e.target.result;
            //send you binary data via $http or $resource or do anything else with it
        }
        r.readAsBinaryString(f);
    }

    $scope.ShowEditOptions = function ($event,$element) {
    
         $scope.affectedControlId = $event.target.id;
         $scope.editorElementId = "Editor" + $scope.affectedControlId;
         $scope.hideAllEditors();
         $scope.showEditor($scope.editorElementId);
         
    }
     


    $scope.showEditor = function (id) {
            
            switch (id) {
                case 'EditorIdLogo':
                    $scope.NgShowEditorIdLogo = true;
                    break;
                case 'EditorIdHome':
                    $scope.NgShowEditorIdHome = true;
                    break;
                default:
            }
    }

    $scope.hideAllEditors = function () {
    	$scope.NgShowEditorIdLogo = false;
        $scope.NgShowEditorIdHome = false;        
    }

    $scope.showUsingNgShow = function (id) {

         var e = angular.element(document.querySelector('#EditorIdHome'))[0];
         var v = e.getAttribute('ng-show');
         e.setAttribute('ng-show', !v);
         var v2 = e.getAttribute('ng-show');

         $compile(e)(scope);

         $scope.$apply();
    }    
});


myAppModule.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/route1', {
                    templateUrl: 'angular-route-template-1.jsp',
                    controller: 'MainCtrl'
                }).
                when('/route2', {
                    templateUrl: 'angular-route-template-2.jsp',
                    controller: 'MainCtrl'
                }).
                otherwise({
                    redirectTo: '/'
                });
        }]);


myAppModule.directive("myMethod",function($parse) {
   
        });

