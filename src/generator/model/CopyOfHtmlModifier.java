package generator.model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class CopyOfHtmlModifier {
	
	public static void updateHtmls(String modifierFilePath, String deployedFilePath, String allChangesXml) 
			throws IOException, ParserConfigurationException, SAXException, 
			XMLStreamException, XPathExpressionException, TransformerException {
	
	 updateSingleHtml(modifierFilePath,allChangesXml);
	// updateSingleHtml(deployedFilePath,allChangesXml);
	}
	
	
	public static void updateSingleHtml(String htmlFilePath,  String xmlRecords) 
			throws IOException, ParserConfigurationException, SAXException, 
			XMLStreamException, XPathExpressionException, TransformerException {
		
		DocumentBuilder dbFile = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputStream htmlFileStream = new FileInputStream(htmlFilePath);
	    Document docDestFile = dbFile.parse(htmlFileStream);  
	    
	    
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
	    is.setCharacterStream(new StringReader(xmlRecords));
        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("element");
        
        for (int i = 0; i < nodes.getLength(); i++) 
        {
        	Element element = (Element) nodes.item(i);
        	NamedNodeMap atts = element.getAttributes();
        	String elementId =  element.getAttribute("id");
        
        	// From Html find the element with matching id
        	Node fileNode = getNodeByIdentifier(elementId,docDestFile );
        	for(int j=0; j<atts.getLength(); j++)
        	{
        		String attributeName = atts.item(j).getNodeName();
        		String attributeValue = atts.item(j).getNodeValue();
        		
        		setAttributeValueForNode(fileNode, attributeName, attributeValue);
        	}
        }
        saveHtmlFile(docDestFile, htmlFilePath);
  }
	
	

public static void saveHtmlFile(Document doc, String filePath) throws IOException, TransformerException {
	DOMSource domSource = new DOMSource(doc);
    StringWriter writer = new StringWriter();
    StreamResult result = new StreamResult(writer);
    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer transformer = tf.newTransformer();
    transformer.transform(domSource, result);
    String finalString =  writer.toString();
	/*
	doc.getTextContent()getDocumentElement().normalize();
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(doc);
    StreamResult result = new StreamResult(new File(filePath));
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.transform(source, result);
*/}

public static Node getNodeByIdentifier(String identifierValue, Document doc ) 
		throws IOException, ParserConfigurationException, SAXException, 
		XMLStreamException, XPathExpressionException {
	XPath xPath =  XPathFactory.newInstance().newXPath();
	String expression = "//*[@id='"+identifierValue+"']"; //"//person[firstname='Lars']/lastname/text()"
	XPathExpression xpathexp = xPath.compile(expression);
	
	NodeList nodeList = (NodeList) xpathexp.evaluate(doc, XPathConstants.NODESET);
	if(nodeList.getLength() >0)
	{
		return nodeList.item(0);
	}
	else 
	{
		return 	null;
	}
 }

public static void setAttributeValueForNode(Node destNode, String attributeName, String newValue )
		throws IOException, ParserConfigurationException, SAXException, XMLStreamException, XPathExpressionException {
	Element destElement = (Element) destNode;
	if(newValue.compareTo("undefined") == 0){
		return;
	}
	
	if(attributeName.compareTo("innertext")!= 0){
 			Attr matchingDestAttribute =  destElement.getAttributeNode(attributeName);
 			if(matchingDestAttribute != null )
 			{
 				matchingDestAttribute.setValue(newValue);
 			} 		
	}
 	else{
 		if(newValue.trim().isEmpty()){
 			destElement.setTextContent("Provide Later");
 		}
 		else { 		
 			destElement.setTextContent(newValue);
 		}
 	}		
 }

  public static String getCharacterDataFromElement(Element e) {
	  Node child = e.getFirstChild();
	  if (child instanceof CharacterData) {
		  CharacterData cd = (CharacterData) child;
		  return cd.getData();
	  }
	  return "";
  }
}