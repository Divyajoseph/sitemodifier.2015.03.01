package generator.model;

import java.io.File;


// This methods need to consult database or folder management logic 
// and return the folder path and file path for creating the copies, 
// modifying working set and publishing 
public class ClientAssetDao {
	static String TemplateFolder = "C:/EclipseJee/FirstApp/WebContent/Templates/";
	static String WorkingFolder = "C:/EclipseJee/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/FirstApp/WorkingFolders/";
	static String EditingFolderUrl = "WorkingFolders/";

	

	public static String getTemplateFolderPath(String templateId){
		return TemplateFolder.concat(templateId);
			}
	
	public static String getWorkingFolderPath(String userId){	
		return WorkingFolder.concat(userId);
	}

	public static String getModifyingFilePath(String userId){	
		return WorkingFolder.concat(userId)+"/modifier.html";
	}
	
	public static String getDeployedFilePath(String userId){	
		return WorkingFolder.concat(userId)+"/final.html";	
	}
	
	public static String getModifyingFileUrl(String userId){	
		return EditingFolderUrl.concat(userId)+"/modifier.html";
	}
	
	public static String getDeployedFileUrl(String userId){	
		return EditingFolderUrl.concat(userId)+"/final.html";	
	}
	
}
