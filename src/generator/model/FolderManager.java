package generator.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FolderManager {
	
	public static void copyFolder(String srcPath, String destPath) throws IOException {
		File src = new File(srcPath);
		File dest = new File(destPath);
		copyFolderAndFiles(src,dest);
	
	}
		
	public static void copyFolderAndFiles(File src, File dest) throws IOException {
		
		 if (dest.exists()) {
		    	dest.delete();
		    }
		  if (src.isDirectory()) {
		    dest.mkdir();
		  
		    String files[] = src.list();
		    for (String file : files) {
		      File srcFile = new File(src, file);
		      File destFile = new File(dest+"\\"+file);
		     
		      copyFolderAndFiles(srcFile,destFile); //recursive copy
		    }
		  } 
		  else { //if file, then copy it
			  System.out.println(src.toPath());
			  System.out.println(dest.toPath());
			  
			  Files.copy(src.toPath(), dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		  }
		}
	
	
	
	
	
		
}
	
