package generator.model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.xml.internal.bind.v2.runtime.XMLSerializer;

public class HtmlModifier {
	
	public static void updateHtmls(String modifierFilePath, String deployedFilePath, String allChangesXml) 
			throws IOException, ParserConfigurationException, SAXException, 
			XMLStreamException, XPathExpressionException, TransformerException {
	
	 updateSingleHtml(modifierFilePath,allChangesXml);
	 updateSingleHtml(deployedFilePath,allChangesXml);
	}
	
	public static Document creatXmlDocument(String htmlFilePath) 
			throws IOException, ParserConfigurationException, SAXException, 
			XMLStreamException, XPathExpressionException, TransformerException {
		try
		{		
			DocumentBuilder dbFile = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputStream htmlFileStream = new FileInputStream(htmlFilePath);
	    	Document docDestFile = dbFile.parse(htmlFileStream);  
	    	return docDestFile;
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	
	public static void updateSingleHtml(String htmlFilePath,  String xmlRecords) 
			throws IOException, ParserConfigurationException, SAXException, 
			XMLStreamException, XPathExpressionException, TransformerException {

		
	    Document docDestFile =  creatXmlDocument(htmlFilePath); 
	    if(docDestFile == null)
	    {
	    	docDestFile =  creatXmlDocument(htmlFilePath +".xml.html"); 	    
	    }
	    if(docDestFile == null)
	    { 
	    	throw new ParserConfigurationException("Could not read xml");
	    }
	    
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
	    is.setCharacterStream(new StringReader(xmlRecords));
        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("element");
        
        for (int i = 0; i < nodes.getLength(); i++) 
        {
        	Element element = (Element) nodes.item(i);
        	NamedNodeMap atts = element.getAttributes();
        	String elementId =  element.getAttribute("id");
        
        	// From Html find the element with matching id
        	Node fileNode = getNodeByIdentifier(elementId,docDestFile );
        	if(fileNode==null){
        		System.out.println(elementId + " is not present in htmlFilePath");
        		continue;
        	}
        		
        	for(int j=0; j<atts.getLength(); j++)
        	{
        		String attributeName = atts.item(j).getNodeName();
        		String attributeValue = atts.item(j).getNodeValue();
        		
        		setAttributeValueForNode(fileNode, attributeName, attributeValue);
        	}
        }
        saveHtmlFile(docDestFile, htmlFilePath);
  }
	
	/*
	DOMSource domSource = new DOMSource(doc);
    StringWriter writer = new StringWriter();
    StreamResult result = new StreamResult(writer);
    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer transformer = tf.newTransformer();
    transformer.transform(domSource, result);
    String finalString =  writer.toString();
	*/
   // doc.getDocumentElement();
	
	
/*
	
	TransformerFactory transformerFactory1 = TransformerFactory.newInstance();
	Transformer transformer1 = transformerFactory1.newTransformer();
	DOMSource source1 = new DOMSource(doc);
	StreamResult result1 = new StreamResult(new File(filePath+".Modified1.html"));
	transformer1.transform(source1,result1);
		
*/
	

public static void saveHtmlFile(Document doc, String filePath) throws IOException, TransformerException {
	
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(doc);
    StreamResult result = new StreamResult(new File(filePath + ".xml.html"));
    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
    transformer.transform(source, result);
    
    TransformerFactory transformerFactoryHtml = TransformerFactory.newInstance();
    Transformer transformerHtml = transformerFactoryHtml.newTransformer();
    DOMSource source1 = new DOMSource(doc);
    StreamResult result1 = new StreamResult(new File(filePath));
    transformerHtml.setOutputProperty(OutputKeys.METHOD, "html");
    transformerHtml.transform(source1, result1);
}

	
	
public static void saveHtmlFileTrail(Document doc, String filePath) throws IOException, TransformerException {
	
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(doc);
    StreamResult result = new StreamResult(new File(filePath + ".xml.html"));
    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
    transformer.transform(source, result);
    
    TransformerFactory transformerFactoryHtml = TransformerFactory.newInstance();
    Transformer transformerHtml = transformerFactoryHtml.newTransformer();
    DOMSource source1 = new DOMSource(doc);
    StreamResult result1 = new StreamResult(new File(filePath+".html.html"));
    transformerHtml.setOutputProperty(OutputKeys.METHOD, "html");
    transformerHtml.transform(source1, result1);
    
    Transformer transformerNone =  TransformerFactory.newInstance().newTransformer();
    DOMSource sourceNone = new DOMSource(doc);
    StreamResult resultNone = new StreamResult(new File(filePath+".none.html"));
    transformerNone.setOutputProperty(OutputKeys.INDENT, "yes");
    transformerNone.transform(sourceNone, resultNone);
    
    Transformer transformertext=  TransformerFactory.newInstance().newTransformer();
    DOMSource sourceText = new DOMSource(doc);
    StreamResult resultText = new StreamResult(new File(filePath+".text.html"));
    transformertext.setOutputProperty(OutputKeys.INDENT, "yes");
    transformertext.transform(sourceText, resultText);
    
}

public static Node getNodeByIdentifier(String identifierValue, Document doc ) 
		throws IOException, ParserConfigurationException, SAXException, 
		XMLStreamException, XPathExpressionException {
	XPath xPath =  XPathFactory.newInstance().newXPath();
	String expression = "//*[@id='"+identifierValue+"']"; //"//person[firstname='Lars']/lastname/text()"
	XPathExpression xpathexp = xPath.compile(expression);
	
	NodeList nodeList = (NodeList) xpathexp.evaluate(doc, XPathConstants.NODESET);
	if(nodeList.getLength() >0)
	{
		return nodeList.item(0);
	}
	else 
	{
		return 	null;
	}
 }

public static void setAttributeValueForNode(Node destNode, String attributeName, String newValue )
		throws IOException, ParserConfigurationException, SAXException, XMLStreamException, XPathExpressionException {
	Element destElement = (Element) destNode;
	if(newValue.compareTo("undefined") == 0){
		return;
	}
	
	if(attributeName.compareTo("innertext")!= 0){
 			Attr matchingDestAttribute =  destElement.getAttributeNode(attributeName);
 			if(matchingDestAttribute != null )
 			{
 				matchingDestAttribute.setValue(newValue);
 			} 		
	}
 	else{
 		if(newValue.trim().isEmpty()){
 			destElement.setTextContent("Provide Later");
 		}
 		else { 		
 			destElement.setTextContent(newValue);
 		}
 	}		
 }

  public static String getCharacterDataFromElement(Element e) {
	  Node child = e.getFirstChild();
	  if (child instanceof CharacterData) {
		  CharacterData cd = (CharacterData) child;
		  return cd.getData();
	  }
	  return "";
  }
}