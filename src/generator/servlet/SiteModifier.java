package generator.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import generator.model.*;
/**
 * Servlet implementation class SiteModifier
 */

@WebServlet("/SiteModifier")
public class SiteModifier extends HttpServlet {
  private static final long serialVersionUID = 1L;


  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	  HttpSession session = request.getSession(true);
		  
	  String userId = (String) session.getAttribute("userId");
	  
	  if(userId==null)
	  {
		  response.sendRedirect("index.html");
		  return;
	  }
	  String modifierFileUrl = ClientAssetDao.getModifyingFileUrl(userId);
	  session.setAttribute("modifierFileUrl", modifierFileUrl);
	  
	  response.sendRedirect(modifierFileUrl);	  
  }

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	
	  try
	  {
		  	 HttpSession session = request.getSession(true);
			 String userId = 	 (String) session.getAttribute("userId");
			 String modifierFileUrl = (String) session.getAttribute("modifierFileUrl");
				
			 if(userId==null || modifierFileUrl ==null)
			 {
				 response.sendRedirect("index.html");
				 return;		
			 }
	 
 
			 String allChangesXml = request.getParameter("allChangesXml");
			 String deployedFilePath = ClientAssetDao.getDeployedFilePath(userId);
			 String modifierFilePath = ClientAssetDao.getModifyingFilePath(userId);
	 
			 HtmlModifier.updateHtmls(modifierFilePath, deployedFilePath, allChangesXml);
			 String modifyingFileUrl = ClientAssetDao.getModifyingFileUrl(userId);
			 response.sendRedirect(modifyingFileUrl);
	  }
	  catch (Exception ex)
	  {
		  throw new ServletException(ex);
	  }	 
 }
 
 
  
  
  @Override
  public void init() throws ServletException {
  
    try {
     
    } catch (Exception e) {
      getServletContext().log("An exception occurred ", e);
      throw new ServletException("An exception occurred"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
   
    	
    	
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 