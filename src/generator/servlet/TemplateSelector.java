package generator.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

 
import generator.model.*;

/**
 * Servlet implementation class TemplateSelector
 */

@WebServlet("/TemplateSelector")
public class TemplateSelector extends HttpServlet {
  private static final long serialVersionUID = 1L;


  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	
	    response.setContentType("text/html");
	    response.sendRedirect("TemplateSelector.html");
	 
  }

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {	  
	 String userId = request.getParameter("userId");
	 String   templateId = request.getParameter("templateId");
	  templateSelected(userId,templateId);
	 HttpSession session = request.getSession(true);
	 String modifierFileUrl = ClientAssetDao.getModifyingFileUrl(userId);
	 
	 session.setAttribute("userId", userId);
	 session.setAttribute("templateId", templateId);
	
	 session.setAttribute("modifierFileUrl", modifierFileUrl);
	 response.sendRedirect( modifierFileUrl); 
  
  }
  
  private void  templateSelected(String userId, String templateId) throws IOException
  {
	FolderManager.copyFolder(ClientAssetDao.getTemplateFolderPath(templateId),
			ClientAssetDao.getWorkingFolderPath(userId));
	
  }
  
  
  
  @Override
  public void init() throws ServletException {
  
    try {
     
    } catch (Exception e) {
      getServletContext().log("An exception occurred ", e);
      throw new ServletException("An exception occurred"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
   
    	
    	
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 